FROM matejturay/tensorpython:latest

WORKDIR /api

COPY ./requirements.txt /api/requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

EXPOSE 5000

RUN chmod 644 app.py

CMD gunicorn -b 0.0.0.0:$PORT server:app