import cv2
import numpy as np
import pytesseract
from PIL import Image, ImageEnhance, ImageFilter
from ocr import preprocess_image_using_opencv, binarize_image_using_opencv
import uuid

import time
import glob
import os, re
import subprocess


def purge(dir, pattern):
    for f in os.listdir(dir):
        if re.search(pattern, f):
            os.remove(os.path.join(dir, f))


counts = {}

for filepath in glob.iglob("./input/*.png"):

    image = cv2.imread(filepath)

    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    thresh0 = cv2.adaptiveThreshold(
        s, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh1 = cv2.adaptiveThreshold(
        v, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh2 = cv2.adaptiveThreshold(
        v, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh = cv2.bitwise_or(thresh0, thresh1)

    # find contours
    cv2.imwrite("test.png", thresh2)

    ctrs, hier = cv2.findContours(
        thresh2.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    # sort contours
    sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
    output = []
    for i, ctr in enumerate(sorted_ctrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)

        # Getting ROI
        roi = image[y : y + h, x : x + w]

        # show ROI
        # cv2.imshow('segment no:'+str(i),roi)
        cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 255), 0)

        if w < 70 and 25 < h < 70:
            cv2.imwrite(f"./output{i}.png", roi)
            try:
                binarize_image_using_opencv(f"./output{i}.png")
                preprocess_image_using_opencv(f"./output{i}.png")
                text = pytesseract.image_to_string(
                    Image.open(f"./output{i}.tif"),
                    config="-c tessedit_char_whitelist=bph --psm 10 --oem 1 -l eng",
                )
                if len(text) == 1:
                    output.append(text)

                    save_path = os.path.join("./dataset_outliers", text)

                    # if the output directory does not exist, create it
                    if not os.path.exists(save_path):
                        os.makedirs(save_path)

                    # write the letter image to a file
                    count = counts.get(text, 1)
                    p = os.path.join(
                        save_path, "{}.png".format(str(count).zfill(6))
                    )
                    cv2.imwrite(p, roi)

                    # increment the count for the current key
                    counts[text] = count + 1

                    purge("./", r"^output")
                    purge("./", r"^input-NEAREST")
            except Exception as e:
                print(e)

    print(output)

subprocess.call(["shutdown", "/s"])
