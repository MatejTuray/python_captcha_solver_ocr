import os
from flask import Flask, flash, request, redirect, url_for, jsonify, abort
from werkzeug.utils import secure_filename
from ocr import fast_ocr
from test import test
from keras.models import load_model
import tensorflow as tf
import pickle
from helpers import purge
from flask_cors import CORS

UPLOAD_FOLDER = "./"
ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg"])


MODEL_FILENAME = "captcha_model.hdf5"
MODEL_LABELS_FILENAME = "model_labels.dat"
CAPTCHA_IMAGE_FOLDER = "generated_captcha_images"


def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    CORS(app)

    def load_keras_model():
        global model
        with open(MODEL_LABELS_FILENAME, "rb") as f:
            lb = pickle.load(f)

        # load the pre-trained Keras model (here we are using a model
        # pre-trained on ImageNet and provided by Keras, but you can
        # substitute in your own networks just as easily)
        try:
            model = load_model(MODEL_FILENAME)
            global graph
            graph = tf.get_default_graph()

        except Exception as e:
            print(e)

    def allowed_file(filename):
        return (
            "." in filename
            and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS
        )

    @app.before_request
    def log_request():
        app.logger.debug("Request Body %s", request.headers)
        app.logger.debug("Request Body %s", request.files)
        app.logger.debug("Request Body %s", request.form)

        return None

    @app.route("/ocr", methods=["POST"])
    def ocr():

        if "file" not in request.files:
            response = jsonify(result="No file part")
            response.status_code = 404
            return response
        file = request.files["file"]

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            whitelist = request.args.get("whitelist")
            file_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            file.save(file_path)
            if whitelist is not None:
                result = fast_ocr(file_path, whitelist=whitelist)
            else:
                result = fast_ocr(file_path, whitelist="everything")
            return jsonify(result=result)
        response = jsonify(result="Invalid file")
        response.status_code = 500
        return response

    @app.route("/cnn", methods=["POST"])
    def cnn():
        if "file" not in request.files:
            response = jsonify(result="No file part")
            response.status_code = 404
            return response
        file = request.files["file"]

        if file.filename == "":
            return jsonify(result="No selected file")
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            file.save(file_path)
            with graph.as_default():
                result = test(file_path)
                purge("./", filename)
                return jsonify(result=result)
        response = jsonify(result="Invalid file")
        response.status_code = 500
        return response

    load_keras_model()
    return app


if __name__ == "__main__":

    app = create_app("config")
    app.run(host="0.0.0.0", debug=True)
