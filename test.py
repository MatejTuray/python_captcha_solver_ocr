from keras.models import load_model
from helpers import resize_to_fit, purge
from imutils import paths
import numpy as np
import imutils
import cv2
import pickle
from PIL import Image, ImageEnhance, ImageFilter
from ocr import preprocess_image_using_opencv, binarize_image_using_opencv
import uuid
import time
import glob
import os, re
import subprocess

MODEL_FILENAME = "captcha_model.hdf5"
MODEL_LABELS_FILENAME = "model_labels.dat"
CAPTCHA_IMAGE_FOLDER = "generated_captcha_images"


# Load up the model labels (so we can translate model predictions to actual letters)
try:
    with open(MODEL_LABELS_FILENAME, "rb") as f:
        lb = pickle.load(f)

    # Load the trained neural network
    model = load_model(MODEL_FILENAME)
except Exception as e:
    print(e)


def test(input_image):

    image = cv2.imread(input_image)

    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    thresh0 = cv2.adaptiveThreshold(
        s, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh1 = cv2.adaptiveThreshold(
        v, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh2 = cv2.adaptiveThreshold(
        v, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2
    )
    thresh = cv2.bitwise_or(thresh0, thresh1)

    # find contours
    cv2.imwrite("test.png", thresh2)

    ctrs, hier = cv2.findContours(
        thresh2.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    # sort contours
    sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
    predictions = []
    for i, ctr in enumerate(sorted_ctrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)

        # Getting ROI
        roi = image[y : y + h, x : x + w]

        # show ROI
        # cv2.imshow('segment no:'+str(i),roi)
        cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 255), 0)

        if w < 100 and 25 < h < 100:
            cv2.imwrite(f"./output{i}.png", roi)
            try:
                binarize_image_using_opencv(f"./output{i}.png")
                preprocess_image_using_opencv(f"./output{i}.png")
                save_path = os.path.join("./result")

                # if the output directory does not exist, create it
                if not os.path.exists(save_path):
                    os.makedirs(save_path)

                # write the letter image to a file
                p = os.path.join(save_path, f"output{i}.png")

                cv2.imwrite(p, roi)
                letter_image = cv2.imread(f"output{i}.png")

                letter_image = resize_to_fit(letter_image, 20, 20)

                letter_image = np.expand_dims(letter_image, axis=0)

                # Ask the neural network to make a prediction
                prediction = model.predict(letter_image)

                # Convert the one-hot-encoded prediction back to a normal letter
                letter = lb.inverse_transform(prediction)[0]
                predictions.append(letter)
                purge("./", r"^output")
                purge("./", r"^input-NEAREST")
                purge("./result", r"^output")
                purge("./", r"^captcha.png")

            except Exception as e:
                print(e)
    captcha_text = "".join(predictions)
    return captcha_text
