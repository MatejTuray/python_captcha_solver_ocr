import pytest
from io import BytesIO
from app import load_keras_model
from werkzeug.datastructures import FileStorage

file = None
load_keras_model()


def test_no_file(client):
    response = client.post("/cnn")
    assert response.json == {"result": "No file part"}
    assert response.status_code == 404


def test_bad_extension(client):

    data = dict(file=(BytesIO(b"test"), "./test.txt"))
    content_type = "multipart/form-data"
    response = client.post("/cnn", data=data, content_type=content_type)
    assert response.status_code == 500
    assert response.json == {"result": "Invalid file"}


def test_cnn(client):
    with open("./tests/cnn.png", "rb") as img:
        data = dict(file=img)
        content_type = "multipart/form-data"
        response = client.post("/cnn", data=data, content_type=content_type)

        assert response.status_code == 200
        assert response.json == {"result": "8UMN"}

