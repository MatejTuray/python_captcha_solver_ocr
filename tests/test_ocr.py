import pytest
from io import BytesIO
from werkzeug.datastructures import FileStorage

file = None


def test_no_file(client):
    response = client.post("/ocr")
    assert response.json == {"result": "No file part"}
    assert response.status_code == 404


def test_bad_extension(client):

    data = dict(file=(BytesIO(b"test"), "./test.txt"))
    content_type = "multipart/form-data"
    response = client.post("/ocr", data=data, content_type=content_type)
    assert response.status_code == 500
    assert response.json == {"result": "Invalid file"}


def test_ocr(client):
    with open("./numeric.png", "rb") as img:
        data = dict(file=img)
        content_type = "multipart/form-data"
        response = client.post("/ocr", data=data, content_type=content_type)

        assert response.status_code == 200
        assert response.json == {"result": "1870"}


def test_no_parse(client):
    with open("./numeric.png", "rb") as img:
        data = dict(file=img)
        content_type = "multipart/form-data"
        response = client.post(
            "/ocr?whitelist=lowercase", data=data, content_type=content_type
        )

        assert response.status_code == 200
        assert response.json == {"result": ""}
