import cv2
import pytesseract
from PIL import Image, ImageEnhance, ImageFilter
import numpy as np

type_dict = {
    "numeric": "0123456789",
    "lowercase": "abcdefghijklmnopqrstuvwxyz",
    "uppercase": "ABCDEFGHIJKLMNOPRQSTUVWXYZ",
    "lowercase_numeric": "abcdefghijklmnopqrstuvwxyz0123456789",
    "uppercase_numeric": "0123456789ABCDEFGHIJKLMNOPRQSTUVWXYZ",
    "upper_and_lower": "ABCDEFGHIJKLMNOPRQSTUVWXYZabcdefghijklmnopqrstuvwxyz",
    "everything": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789",
}


def binarize_image_using_opencv(captcha_path):
    im_gray = cv2.imread(captcha_path, cv2.IMREAD_GRAYSCALE)
    (thresh, im_bw) = cv2.threshold(im_gray, 85, 255, cv2.THRESH_BINARY)
    # although thresh is used below, gonna pick something suitable
    im_bw = cv2.threshold(im_gray, thresh, 255, cv2.THRESH_BINARY)[1]
    cv2.imwrite(captcha_path, im_bw)

    return captcha_path


def preprocess_image_using_opencv(captcha_path):

    bin_image_path = captcha_path
    kernel = np.ones((5, 5), np.float32) / 25
    name = captcha_path.replace(".png", "")
    im_bin = Image.open(bin_image_path)
    basewidth = 300  # in pixels
    wpercent = basewidth / float(im_bin.size[0])
    hsize = int((float(im_bin.size[1]) * float(wpercent)))
    big = im_bin.resize((basewidth, hsize), Image.NEAREST)

    # tesseract-ocr only works with TIF so save the bigger image in that format
    tif_file = "input-NEAREST.tif"
    big.save(tif_file)

    im = Image.open("input-NEAREST.tif")  # the second one

    im = im.filter(ImageFilter.MedianFilter())
    enhancer = ImageEnhance.Contrast(im)
    enh = ImageEnhance.Sharpness(im)
    im = enh.enhance(2)
    im = im.filter(ImageFilter.SHARPEN)
    im = enhancer.enhance(2000)
    im = im.convert("1")

    im.save(f"{name}.tif")


def get_captcha_text_from_captcha_image(captcha_path):

    # Preprocess the image befor OCR
    tif_file = preprocess_image_using_opencv(captcha_path)


def fast_ocr(input_path, whitelist):

    text = pytesseract.image_to_string(
        Image.open(input_path),
        config=f"-c tessedit_char_whitelist={type_dict[whitelist]} --psm 7 --oem 1 -l eng",
    )
    return text
